package com.treeunfe.empresasteste.util;

public class MConstants {

    public static final String BASE_URL = "https://empresas.ioasys.com.br/api/v1/";
    public static final String IMAGES_URL = "https://empresas.ioasys.com.br/";
}