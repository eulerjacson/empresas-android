package com.treeunfe.empresasteste.entity;

public class EnterpriseType {
    private float id;
    private String enterprise_type_name;

    // Getter Methods
    public float getId() {
        return id;
    }

    public String getEnterpriseTypeName() {
        return enterprise_type_name;
    }

    // Setter Methods
    public void setId(float id) {
        this.id = id;
    }

    public void setEnterprise_type_name(String enterprise_type_name) {
        this.enterprise_type_name = enterprise_type_name;
    }
}
